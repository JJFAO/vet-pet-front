import React from "react";
import Routes from './Routes';

export default class App extends React.Component {
  render() {
    return (
     <React.Fragment>
         <Routes />
     </React.Fragment>
    );
  }
}
