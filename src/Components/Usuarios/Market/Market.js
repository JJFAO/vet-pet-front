import React, { useState, useEffect, useCallback } from "react";
import { Link } from "react-router-dom";

import './Market.css';
import Pagination from "./../Pagination/Pagination";

export default function Market() {

  const [shopItems, setShopItems] = useState({
    totalpages: 0,
    quantity: 6,
    page: 0,
  });
  const [itemAmount, setItemAmount] = useState(1)
  const [coleccionCosas, setIts] = useState( JSON.parse(localStorage.getItem("CarritoItems")) || [] );

  const handlePagination = useCallback((i = 0) => {
   
    fetch(`http://localhost:4000/products/search?quantity=${shopItems.quantity}&page=${i}`)
      .then(resp => resp.json())
      .then(data => {
        setShopItems({
          prods: data.prods,
          totalpages: data.totalpages,
          quantity: 6,
          page: i,
        });
      });
  },[shopItems.quantity])

  useEffect(() => {
    handlePagination()
  },[handlePagination]);


  const handleInput = (e) => {
      const value = e.target.value;
      setItemAmount(value)
  }
    

  

  function handleClick(id) {
    let resultadoFind = shopItems.prods.find((item) => {
      return item._id === id;
    });
    resultadoFind.quantity = itemAmount
    coleccionCosas.push(resultadoFind);
    save(coleccionCosas);
  }

  function save(it) {
    localStorage.setItem('CarritoItems', JSON.stringify(it));
    setIts(JSON.parse(localStorage.getItem("CarritoItems"))); 
  }

  const handleDeleteCart = (id) =>{
    let coleccionCosas = JSON.parse(localStorage.getItem("CarritoItems")) || [];
    let deleteExist = coleccionCosas.filter(item => {
      return item._id !== id;
    });
    localStorage.setItem("CarritoItems", JSON.stringify(deleteExist));
    setIts(JSON.parse(localStorage.getItem("CarritoItems"))); 
  }


  function isAdded(_id) {
    let exist = coleccionCosas.find((item) => {
      return item._id === _id;
    });
      
      return exist ? true : false
      
  }

  const ShopItemsArray = Array.isArray(shopItems.prods) && shopItems.prods.map((item, i) => {
    let a = ''
    if (isAdded(item._id)) {
      a = <button onClick={() => {handleDeleteCart(item._id); return false}}
            className="slide-top align-self-center border-0 p-0 bg-white">
            <img alt="icono agregado" title="Item Added to the Cart :D"
              src="https://img.icons8.com/color/40/000000/checked-2.png"/>
          </button>;
    } else {
      a = <button onClick={() => {handleClick(item._id); return false}}
            className="slide-in-bottom aling-self-center border-0 p-0 bg-white">
            <img alt="icono carrito" title="Add to the cart"
              src="https://img.icons8.com/officel/40/000000/shopping-cart-loaded.png"/>
          </button>;
    }
   return <div key={i} className="col-9 col-md-4 my-3">
    {item.discount
      ? <div className="discount-label yellow"> <span>{item.discount} %</span></div>
      : ''
    }
    <div className="card card-style" id="market">
      <div className="text-center card-prods">
      <img alt="imagen de producto" src={item.image} className="card-img-top img-fluid style-img-items"/>
      </div>
      <div className="card-body">
        <div className='row'>
          <div className="col-12">
          <Link to={"/article/" + item._id}><h5 className="card-title">{item.prodName}</h5></Link>
          </div>
          <div className="col-12 mb-2 d-flex justify-content-between input-group-prepend box-Cart">
              {a}
              <select name="quantity" className="form-control inputNumberButtons w-25 text-center" onChange={handleInput}>
                <option value="1" defaultValue>1</option> 
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
              </select>
            </div>
          <div className="col-12">
            <span className="style-price-span box-price">Precio: ${item.price}</span>
          </div>
        </div>
      </div>
    </div>
  </div>
  });

  return (
    <section className='container'>
      <div className='row justify-content-center'>
        <div className='col-12'>
          <div className='justify-content-around container-card row'>
            {ShopItemsArray}
          </div>
        </div>
        <Pagination  pagina={shopItems.page} quantity={shopItems.quantity} totalPages={shopItems.totalpages}  handlePagination={(i) =>{handlePagination(i)}}/>
      </div>
    </section>
  );
} 