import React from "react";
import "./Login.css";
import handleError from "./../../../Helpers/HandleError"
import {Redirect, Link } from "react-router-dom";
import Footer from '../Footer/Footer';
import CommonNavbar from '../CommonNavbar/CommonNavbar';
import Emergencias from "../Emergencias/Emergencias";


class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // alerta: true,
      loggedIn: null,
      loggedInUser: false,
      loggedInAdmin: false
    };
  }

  isLogged = JSON.parse(localStorage.getItem("token"))

  handleInput = e => {
    const { value, name } = e.target;
    this.setState({
      [name]: value
    });
  };


  //  alerts = (hasError) => {
    
  //   if (this.state.alerta ) {
  //     document.getElementById('valid-log').classList.remove('d-none');
  //     hasError = true;
  //   } else {
  //     document.getElementById('valid-log').classList.add('d-none');
  //   }
  //   return hasError;
  // }


  handleClickLogin = e => {
    e.preventDefault();
    const data = {
      email: this.email,
      password: this.password
    };
    const requestInfo = {
      method: 'POST',
      body: JSON.stringify(data),
      headers: new Headers({
        'Content-Type': 'application/json'
      }),
    };
    this.setState({
      loading: "preloader",
      // alerta: true
    });
    let status;

    fetch("http://localhost:4000/login", requestInfo)
      .then((resp) => {
        if (!resp.ok) status = resp.status
        return resp.json()})
        .then(resp => {
          if (status) {
            handleError(resp);
            return;
          }
        this.setState({ loading: "" });
        localStorage.setItem("token", JSON.stringify(resp.token));
        localStorage.setItem("dataUser", JSON.stringify(resp.userresponse));
        this.setState({loggedIn: true });
        
        if(resp.userresponse.role === 'user'){
          this.setState({
           loggedInUser: true
          })
        }
        if (this.state.loggedIn != null && this.state.loggedInUser) {
          this.props.history.push("/")
          return 
        }else {
          this.props.history.push("/admin")
          return;
        }       
    })
      .catch(error => {
        console.log(error);
      });
  };
  

  render() {
    return (
      <React.Fragment>
        {this.isLogged ? <Redirect to="/"/> : ''}
        <Emergencias />
        <CommonNavbar />
        <section className="d-flex justify-content-center py-5">
          <div className="card-border text-center col-12 col-md-6">
            <div className="card-header-log ">
              <p>BIENVENIDO A PATITAS!</p>
            </div>
            <div className="card-body">
              <form action="" className="pt-3 px-3">
                <div id="valid-log" className="text-danger d-none">
                  Tu Contraseña o Email son incorrectos
                </div>
                <input
                  className="form-control p-3 border-radius-form"
                  type="text"
                  name="email"
                  placeholder="E-mail"
                  onChange={e => this.email = e.target.value}
                />
                <input
                  className="form-control mt-4 p-3 border-radius-form"
                  type="password"
                  name="password"
                  placeholder="Contraseña"
                  onChange={e => this.password = e.target.value}
                />
                <div className="d-flex align-items-center mt-3">
                  <input className="mr-2" type="checkbox" />
                  <small>Recordar</small>
                </div>
                <hr />
                <button
                  className="btn btn-primary btn-block mt-4 p-2 border-radius-form mb-3"
                  onClick={this.handleClickLogin}
                >
                  Ingresar
                </button>
                <div className="w-100 d-flex justify-content-center">
                  <div className={this.state.loading}></div>
                </div>
                <div className="text-center mt-4">
                  <span className="d-block">
                    Recuperar contraseña
                  </span>
                  <Link  to="/regist" className="d-block">
                    Registrarme
                  </Link>
                </div>
              </form>
            </div>
            <div className="card-footer text-muted">
              <p></p>
            </div>
          </div>
        </section>
        <Footer />
      </React.Fragment>
    );
  }
}

export default Login;
