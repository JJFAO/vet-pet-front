import React, { Component } from "react";
import "./CommonNavbar.css";
import NavLinksBar from "../NavLinksBar/NavLinksBar.js";
import { Link } from "react-router-dom";

// import Login from "./Login/Login.js";

export default class CommonNavbar extends Component {
  token = JSON.parse(localStorage.getItem('token'));

  logOut = () => {
    localStorage.removeItem("token");
    localStorage.removeItem("dataUser");
    // this.props.userLogged(null);
  };
  userLogged = () => {
    if (this.token) {
      return (
        <div className="col-6 col-md-12 d-none d-md-flex  justify-content-end py-2">
          <Link onClick={this.logOut} className="btn btn-style" to="/">
            Logout
          </Link>
        </div>
      );
    } else {
      return (
        <div className="dropdown d-none d-md-inline-block mx-2">
          <div className=" btn dropbtn">
            <img alt="boton login o registro" src="images/004-objetivo.svg"></img>
          </div>
          <div className="dropdown-content">
            <Link className="btn btn-style dropdown-login" to="/login">
              LogIn
            </Link>
            <Link className="btn btn-style dropdown-login" to="/regist">
              Register
            </Link>
          </div>
        </div>
      );
    }
  };

  render() {
    return (
      <div className="container-fluid shadow-sm  ">
        <nav className="common-navbar row d-flex">
          <div className="common-navbar-logo col-2 pt-2">
            <Link className=" " to="/">
              <img alt="logo home" src="images/LOGO_VETE.svg" />
            </Link>
          </div>
          <div className="justify-content-center col-8 linksnav">
            <NavLinksBar />
          </div>
          <div className="col-2  d-flex justify-content-end common-header-icons pt-2">
            {this.userLogged()}
            <div className=" btn mx-2">
            <Link to="/cart">
              <img alt="icono carrito" style={{ paddingTop: 10 }} src="images/015-carro-de-la-compra-3.svg"></img>
              </Link>
            </div>
          </div>
        </nav>
      </div>
    );
  }
}
