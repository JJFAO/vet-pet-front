import React, { Component } from "react";
import "./Contacto.css";
import handleError from "../../../Helpers/HandleError";

export default class Contacto extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      inquire: "",
      nombre:""
    };
  }


  handleInput = e => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState({
      [name]: value
    });
  };


  handleSubmit = e => {
    let status;
    e.preventDefault();
    if(this.state.name === "" || this.state.inquire === "" || this.state.email === ""){
       alert("Completa los campos requeridos")
       return
    }

    fetch('http://localhost:4000/faq/addFaq', {
      method: 'POST',
      body: JSON.stringify(this.state),
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then(resp => {
      if (!resp.ok) status = resp.status;
      return resp.json();
    })
    .then(resp => {
      if (status) {
        handleError(resp);
        return;
      }
      alert("Gracias por tu consulta, responderemos a la brevedad");
    })
  };


  render() {
    return (
      <div className="container-fluid" id="contacto">
        <React.Fragment>
          <form onSubmit={this.handleSubmit}>
            <div className="container-fluid contactopart justify-content-center pl-md-5 pb-5">
              <div className="text-center contactopart-title pt-md-4 pt-sm-3 ">
                <p>CONSULTAS</p>
              </div>
              <div className="row mt-5">
                <div className="pt-md-4 pt-sm-2 pr-md-5  col-md-6 text-justify contacto-text">
                  <p>
                    Nuestros clientes y sus mascotas son una parte importante de
                    esta familia "PATITAS", y nos encanta mantenernos en
                    contacto con todos! <br></br>
                    Si tenes alguna consulta, comentario o sugerencia que
                    quieras hacernos, te dejamos este formulario para que nos la
                    hagas llegar.Prometemos ponernos en contacto con vos lo
                    mas rapido posible! <br></br>Si necesitas  un turno para <span>CIRUGIAS, LABORATORIO o CLINICA</span>  te pedimos que tambien nos envies tus datos asi nos
                    ponemos en contacto con vos. <br></br>
                    <br></br>Queremos dedicarles el tiempo y los cuidados que tu
                    mascota se merece.
                  </p>
                  <div className="contacto-firma row float-right  ">
                    <img alt="logo veterinaria patitas" src={"/images/LOGO_VETE.svg"} />
                    <h4 className="ml-3 mr-3"> PATITAS</h4>
                  </div>
                </div>
                <div className="form-group col-md-6 justify-content-sm-around contacto-emergencia pl-md-5">
                <h5 className="mt-2">NOMBRE</h5>
                  <input
                    type="text"
                    name="name"
                    onChange={this.handleInput}
                    className="form-control mailinput mb-2"
                    id="name"                    
                    placeholder="Tu Nombre"
                  /> 
                  <h5>E MAIL</h5>
                  <input
                    type="email"
                    name="email"
                    onChange={this.handleInput}
                    className="form-control mailinput"
                    id="exampleInputEmail1"
                    aria-describedby="emailHelp"
                    placeholder="Enter email"
                  />
                  <small id="emailHelp" className="form-text text-muted mb-2">
                    Esta informacion es privada, jamas será revelada sin su
                    consentimiento.
                  </small>                 
                  <h5>TU CONSULTA</h5>
                  <textarea
                    name="inquire"
                    onChange={this.handleInput}
                    className="form-control contactoinput"
                    id="exampleFormControlTextarea1"
                    rows="3"
                  ></textarea>
                  <p className="col-8 mt-4 ">
                   Recordá que en nuestra direccion tenemos SERVICIO DE
                    URGENCIA 24hs! En la pantalla encontraras el numero de
                    contacto!
                  </p>
                  <div className=" mr-2 text-right">
                    <button
                      type="submit"
                      className="btn sharedbtn mt-2 pr-4 mr-5 style=vertical-align: middle">
                      <span>Enviar </span>
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </React.Fragment>
      </div>
    );
  }
}
