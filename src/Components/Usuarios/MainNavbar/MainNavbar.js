import React from "react";
import { Link } from "react-router-dom";
import "./MainNavbar.css";

export default class MainNavbar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: '',
      carrito:(JSON.parse(localStorage.getItem("CarritoItems")) || []).length
    };
  }
  currentUser = JSON.parse(localStorage.getItem('dataUser'));

  logOut = () => {
    localStorage.removeItem("token");
    localStorage.removeItem("dataUser");
    this.setState({
      user: null
    })
  };


  componentDidMount = () => {
    this.setState({
      user: this.currentUser
    })
  }


  userLogged = () => {
    if (this.state.user) {
      return (
        <>
          <span className=" btn-style mr-2">
            {this.state.user.name}
          </span>
          <div className="dropdown mx-2">
            <div className=" btn dropbtn main">
              <img alt="icono usuario" src="images/004-objetivo.svg"></img>
            </div>
            <div className="dropdown-content">
              <span onClick={this.logOut} className="btn btn-style">
                Logout
              </span>
              <Link to="/admin" className="btn btn-style">
                Admin Dashboard
               </Link>
            </div>
          </div>
        </>
      );
    } else {
      return (
        <div className="dropdown mx-2">
          <div className=" btn dropbtn main">
            <img alt="icono usuario" src="images/004-objetivo.svg"></img>
          </div>
          <div className="dropdown-content">
            <Link to="/login" className="btn btn-style">
              LogIn
            </Link>
            <Link className="btn btn-style" to="/regist">
              Register
            </Link>
          </div>
        </div>
      );
    }
  };


  render() {
    return (
      <div>
        <div className="header container-fluid bg-light mt-4 ">
          <div className="row">
            <div className="col-md-12 mr-4 mr-md-1 order-1 order-md-0 d-flex justify-content-end py-3 align-items-center">
              {this.userLogged()}
              <div className="mainheader-cart btn mx-2">
                <Link to="/cart">
                  <img alt="icono carrito" src="images/015-carro-de-la-compra-3.svg"></img>
                  <span className="badge badge-light">{this.state.carrito}</span>
                  <span className="sr-only">unread messages</span>
                </Link>
              </div>
            </div>
            <div className=" col-sm-12 d-md-flex justify-content-center mt-1 mb-1 mainheader-logo">
              <img alt="logo veterinaria patitas" src={"images/LOGO_VETE.svg"} />
            </div>
            <div className="col-md-12 d-md-flex justify-content-center text-center mt-1">
              <p>PATITAS</p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
