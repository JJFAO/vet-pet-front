import React from "react";
import { useState } from "react";
import "./Regist.css";
import { Redirect } from "react-router-dom";
import swal from "sweetalert";
import handleError from "./../../../Helpers/HandleError";


export default function Regist() {
  
  const isLogged = JSON.parse(localStorage.getItem("token"))
  const [inputs, setInputs] = useState({
    name: "",
    lastName: "",
    email: "",
    dni: "",
    password: "",
    confirmPassword: ""
  });

  
  const handleInputs =  e => {
    let name = e.target.name;
    let newValue = e.target.value;
    setInputs({ ...inputs, [name]: newValue });
    alerts();
  };

  const alerts = hasError => {
    if (inputs.name === "" || inputs.name.length < 2) {
      document
        .getElementById("validationServer01")
        .classList.remove("is-valid");
      document.getElementById("validationServer01").classList.add("is-invalid");
      document.getElementById("valid-feedback01").classList.add("d-none");
      document.getElementById("invalid-feedback01").classList.remove("d-none");
      hasError = true;
    } else {
      document.getElementById("validationServer01").classList.add("is-valid");
      document
        .getElementById("validationServer01")
        .classList.remove("is-invalid");
      document.getElementById("valid-feedback01").classList.remove("d-none");
      document.getElementById("invalid-feedback01").classList.add("d-none");
    }
    if (inputs.lastName === "" || inputs.lastName.length < 2) {
      document
        .getElementById("validationServer02")
        .classList.remove("is-valid");
      document.getElementById("validationServer02").classList.add("is-invalid");
      document.getElementById("valid-feedback02").classList.add("d-none");
      document.getElementById("invalid-feedback02").classList.remove("d-none");
      hasError = true;
    } else {
      document.getElementById("validationServer02").classList.add("is-valid");
      document
        .getElementById("validationServer02")
        .classList.remove("is-invalid");
      document.getElementById("valid-feedback02").classList.remove("d-none");
      document.getElementById("invalid-feedback02").classList.add("d-none");
    }
    if (
      inputs.email === "" ||
      inputs.email.length < 2 ||
      !/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(.\w{2,3})+$/.test(inputs.email)
    ) {
      document
        .getElementById("validationServer03")
        .classList.remove("is-valid");
      document.getElementById("validationServer03").classList.add("is-invalid");
      document.getElementById("valid-feedback03").classList.add("d-none");
      document.getElementById("invalid-feedback03").classList.remove("d-none");
      hasError = true;
    } else {
      document.getElementById("validationServer03").classList.add("is-valid");
      document
        .getElementById("validationServer03")
        .classList.remove("is-invalid");
      document.getElementById("valid-feedback03").classList.remove("d-none");
      document.getElementById("invalid-feedback03").classList.add("d-none");
    }
    if (inputs.dni === "" || inputs.dni.length < 7) {
      document
        .getElementById("validationServer04")
        .classList.remove("is-valid");
      document.getElementById("validationServer04").classList.add("is-invalid");
      document.getElementById("valid-feedback04").classList.add("d-none");
      document.getElementById("invalid-feedback04").classList.remove("d-none");
      hasError = true;
    } else {
      document.getElementById("validationServer04").classList.add("is-valid");
      document
        .getElementById("validationServer04")
        .classList.remove("is-invalid");
      document.getElementById("valid-feedback04").classList.remove("d-none");
      document.getElementById("invalid-feedback04").classList.add("d-none");
    }
    if (inputs.password === "" || inputs.password.length < 4) {
      document
        .getElementById("validationServer05")
        .classList.remove("is-valid");
      document.getElementById("validationServer05").classList.add("is-invalid");
      document.getElementById("valid-feedback05").classList.add("d-none");
      document.getElementById("invalid-feedback05").classList.remove("d-none");
      hasError = true;
    } else {
      document.getElementById("validationServer05").classList.add("is-valid");
      document
        .getElementById("validationServer05")
        .classList.remove("is-invalid");
      document.getElementById("valid-feedback05").classList.remove("d-none");
      document.getElementById("invalid-feedback05").classList.add("d-none");
    }
    if (
      inputs.confirmPassword === "" ||
      inputs.confirmPassword.length < 4 ||
      inputs.password !== inputs.confirmPassword
    ) {
      document
        .getElementById("validationServer06")
        .classList.remove("is-valid");
      document.getElementById("validationServer06").classList.add("is-invalid");
      document.getElementById("valid-feedback06").classList.add("d-none");
      document.getElementById("invalid-feedback06").classList.remove("d-none");
      hasError = true;
    } else {
      document.getElementById("validationServer06").classList.add("is-valid");
      document
        .getElementById("validationServer06")
        .classList.remove("is-invalid");
      document.getElementById("valid-feedback06").classList.remove("d-none");
      document.getElementById("invalid-feedback06").classList.add("d-none");
    }
    return hasError;
  };

  const handleSubmit = e => {
    e.preventDefault();
    let hasError = false;
    hasError = alerts(hasError);
    if (!hasError) {
      fetchRegist();
    }
  };

  function fetchRegist() {
    let status;

    fetch("http://localhost:4000/register", {
      method: "POST",
      body: JSON.stringify({
        name: inputs.name,
        lastName: inputs.lastName,
        email: inputs.email,
        dni: inputs.dni,
        password: inputs.password
      }),
      headers: {
        "Content-Type": "application/json"
      }
    })
    .then(resp => {
      if (!resp.ok) status = resp.status;
      return resp.json();
    })
    .then(res => {
      if (status) {
        handleError(res);
        return;
      }
      if (res) {
        swal("Tu solicitud", "A sido enviada exitosamente!", "success");
        setTimeout(() => {
          window.location.href = "http://localhost:3000/"
        }, 3000)
        return;
      }
    })
  }

  return (
    <React.Fragment>
      {isLogged ? <Redirect to="/"/> : ''}
      <section className="d-flex justify-content-center py-5  ">
        <form
          className="needs-validation  card card-regist card-border col-md-8 col-sm-6"
          onSubmit={handleSubmit}
          noValidate
        >
          <div className="form-row justify-content-center px-2 py-3">
            <div className="card-header-log text-center">
              <p>UNITE A PATITAS</p>
            </div>
            <div className="col-8 mb-1">
              <label htmlFor="validationServer01">Nombre</label>
              <input
                type="text"
                className="form-control is-invalid"
                id="validationServer01"
                placeholder="Nombre..."
                name="name"
                onBlur={alerts}
               onInput={handleInputs}
                required
              />
              <div id="valid-feedback01" className="valid-feedback">Se ve Bien!</div>
              <div id="invalid-feedback01" className="invalid-feedback">Por favor, completá tu Nombre</div>
            </div>
            <div className="col-8 mb-1">
              <label htmlFor="validationServer02">Apellido</label>
              <input
                type="text"
                className="form-control is-invalid"
                id="validationServer02"
                placeholder="Apellido..."
                name="lastName"
               onInput={handleInputs}
               onBlur={alerts}
                required
              />
              <div id="valid-feedback02" className="valid-feedback">Se ve Bien!</div>
              <div id="invalid-feedback02" className="invalid-feedback">Por favor, completá tu Apellido</div>
            </div>
            <div className="col-8 mb-1">
              <label htmlFor="validationServerUsername">Email</label>
              <div className="input-group">
                <div className="input-group-prepend">
                  <span className="input-group-text" id="inputGroupPrepend3">@</span>
                </div>
                <input onInput={handleInputs} name="email" placeholder="Email..." type="email" className="form-control is-invalid" id="validationServer03" aria-describedby="inputGroupPrepend3" onBlur={alerts} required />
                <div id="valid-feedback03" className="valid-feedback">Se ve Bien!</div>
                <div id="invalid-feedback03" className="invalid-feedback">Por favor, completá tu Email</div>
              </div>
            </div>
            <div className="col-8 mb-1">
              <label htmlFor="validationServer03">Contraseña</label>
              <input onInput={handleInputs} onBlur={alerts} name="password" placeholder="Contraseña..." type="password" className="form-control is-invalid" id="validationServer05" required />
              <div id="valid-feedback05" className="valid-feedback">Se ve Bien!</div>
              <div id="invalid-feedback05" className="invalid-feedback">Tu contraseña debe ser de 4 o más caracteres</div>
            </div>
            <div className="col-8 mb-1">
              <label htmlFor="validationServer05">Confirma tu Contraseña</label>
              <input onInput={handleInputs} onBlur={alerts} name="confirmPassword" placeholder="Confirma tu contraseña" type="password" className="form-control is-invalid" id="validationServer06" required />
              <div id="valid-feedback06" className="valid-feedback">Se ve Bien!</div>
              <div id="invalid-feedback06" className="invalid-feedback">Las contraseñas no Coinciden</div>
            </div>
            <div className="col-8 mb-1">
              <label htmlFor="validationServer03">DNI</label>
              <input onInput={handleInputs} onBlur={alerts} name="dni" placeholder="DNI..." type="number" className="form-control is-invalid" id="validationServer04" required />
              <div id="valid-feedback04" className="valid-feedback">Se ve Bien!</div>
              <div id="invalid-feedback04" className="invalid-feedback">Por favor, completá tu DNI</div>
            </div>
            <div className="col-12 text-center mb-2">
              <button
                className="btn btn-primary border-radius-form col-8"
                type="submit"
              >
                {" "}
                ENVIAR{" "}
              </button>
            </div>
          </div>
        </form>
      </section>
    </React.Fragment>
  )
}

