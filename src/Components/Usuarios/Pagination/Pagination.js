import React from "react";
import './Pagination.css';

export default function Pagination(props) {

    let pageButtons = [];
    for (let i = 0; i < props.totalPages; ++i) {

      let active = i === props.pagina ? 'activo' : '';
      pageButtons.push(
        <li key={i} className="page-item">
          <button className={"page-link btn " + active} onClick={() => props.handlePagination(i)}> {i + 1} </button>
        </li>
      )
    }
    let disabledPrev = props.pagina <= 0 ? 'd-none' : '';
    let disabledNext = props.pagina >= props.totalPages -1 ? 'd-none' : '';

    return (
      <div className='mt-3'>
        <nav aria-label="Page navigation">
          <ul className="pagination justify-content-center">
            <li className="page-item">
              <button className={"page-link " + disabledPrev} tabIndex="-1" aria-disabled="true" onClick={() => props.handlePagination(props.pagina - 1)}>Previous</button>
            </li>
            {pageButtons}
            <li className="page-item">
              <button className={"page-link " + disabledNext} onClick={() => props.handlePagination(props.pagina + 1)}>Next</button>
            </li>
          </ul>
        </nav>
      </div>

    )
  }

