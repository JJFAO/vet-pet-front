import React, { useState } from 'react';
import { Link, Redirect } from "react-router-dom";
import { useParams } from 'react-router-dom';
import handleError from "../../../Helpers/HandleError";


export default function EditProduct(props) {
  const { id } = useParams();
  const [item, setItem] = useState()


  const handleDelete = (e) => {
    e.preventDefault();
    const token = JSON.parse(localStorage.getItem('token'));
    let status;

    fetch(`http://localhost:4000/products/delete/${id}`, {
      method: 'DELETE',
      headers: new Headers({
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json',
      }),
    })
      .then(resp => {
        if (!resp.ok) status = resp.status;
        return resp.json();
      })
      .then(resp => {
        if (status) {
          handleError(resp);
          return;
        }
        if (resp) {
          setItem('successfully')
        }
      })
      .catch(error => {
        console.log(error);
      });
  }


  if (item === 'successfully') {
    setTimeout(() => setItem('redirect'), 2000);
  }
  if (item === 'redirect') {
    return <Redirect to={'/admin/myproducts'} />
  }
  if (item === 'successfully') {
    return (
      <div class="container py-5">
        <div class="alert alert-success mt-3 text-center" role="alert">
          El producto  fue eliminado exitósamente! </div>
      </div>
    )
  }
  return (
    <div className="container text-center">
      <div class="card my-5 mx-5">
        <div class="card-body py-5">
          <h3 class="card-title">¿Está seguro que desea eliminar el producto?</h3>
          <button class="btn btn-danger mr-2" onClick={handleDelete}>Confimar</button>
          <Link to={'/admin/myproducts'}><button class="btn btn-info">Volver</button></Link>
        </div>
      </div>
    </div>
  );
};

