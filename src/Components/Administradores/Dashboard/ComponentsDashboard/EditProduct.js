import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import handleError from "../../../../Helpers/HandleError";


export default function EditProduct(props) {
  const { id } = useParams();
  const [inputs, setInputs] = useState({});
  const [confirm, setConfirm] = useState(false);


  useEffect(() => {
    let status;
    fetch(`http://localhost:4000/products/search/oneproduct/${id}`)
      .then(resp => {
        if (!resp.ok) status = resp.status;
        return resp.json();
      })
      .then(resp => {
        if (status) {
          handleError(resp);
          return;
        }
        setInputs(resp);
        console.log(resp.prodName);
        
      })
      .catch(error => {
        console.log(error);
      });
  }, [id])


  const handleInput = (e) => {
    const name = e.target.name;
    const value = e.target.type === 'checkbox' ? e.target.checked : e.target.value;
    setInputs({ ...inputs, [name]: value });
  }


  const handleEditProduct = (e) => {
    e.preventDefault();
    const token = JSON.parse(localStorage.getItem('token'));
    const requestInfo = {
      method: 'PUT',
      body: JSON.stringify(inputs),
      headers: ({
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json',
      }),
    };
    let status;
    fetch(`http://localhost:4000/products/modify?id=${id}`, requestInfo)
      .then(resp => {
        if (!resp.ok) status = resp.status;
        return resp.json();
      })
      .then(resp => {
        if (status) {
          handleError(resp);
          return;
        }
        if (resp) {
          setConfirm(true)
        }
      })
      .catch(error => {
        console.log(error);
      });
  }


  setTimeout(() => setConfirm(false), 6000);


  return (
    <div>
      <div className="card-body py-5">
        <form className="px-lg-5 w-lg-80" onSubmit={handleEditProduct}>
          <div className="w-100 text-center">
            <h2>Editar producto</h2>
          </div>
          {
            confirm ? (
              <div className="alert alert-success mt-3 text-center" role="alert">
                El producto {inputs.prodName} Editado exitósamente! </div>
            ) : ''
          }

          <div className="form-group">
            <label>Titulo</label>
            <input defaultValue={inputs.prodName} type="text" className='form-control' name="prodName" onChange={handleInput} />
          </div>
          <div className="form-group">
            <label>Imagen</label>
            <input defaultValue={inputs.image} type="text" className='form-control' name="image" onChange={handleInput} />
          </div>
          <div className="form-group">
            <label>Descuento</label>
            <input defaultValue={inputs.discount} type="number" className='form-control' name="discount" onChange={handleInput} />
          </div>
          <div className="form-group">
            <label>Precio</label>
            <input defaultValue={inputs.price} type="number" className='form-control' name="price" onChange={handleInput} />
          </div>
          <div className="form-group">
            <label>Stock disponible</label>
            <input defaultValue={inputs.stock} type="number" className='form-control' name="stock" onChange={handleInput} />
          </div>
          <div className="form-group form-row mt-4">
            <label className="checkbLabel">Producto Destacado</label>
            <input type="checkbox" className='form-control checkb' name="featured" onChange={handleInput} />
          </div>
          <button className="btn btn-success">Editar</button>
        </form>
      </div>
    </div>
  );
};

